﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kordamine0203
{
    class Inimene
    {
        public string Nimi;
        public string Isikukood;

        public DateTime Sünniaeg()
        {
            int sajand = 1800;
            switch (Isikukood.Substring(0, 1))
            {
                case "3": case "4": sajand = 1900; break;
                case "5": case "6": sajand = 2000; break;
            }

            return new DateTime(

                sajand +

                int.Parse(Isikukood.Substring(1, 2)),     // siia tuleks panna aasta
                int.Parse(Isikukood.Substring(3, 2)),     // siia tuleks panna kuu
                int.Parse(Isikukood.Substring(5, 2))      // siia atuleks panna päev
                );
        }

        public int Vanus() => (DateTime.Today - Sünniaeg()).Days * 4 / 1461;


        public override string ToString()
        => $"{Isikukood} - {Nimi}";
    }

}
